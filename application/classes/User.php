<?php

class User extends Vds_User {

	public static function load($phone)
	{
		$orm_user = ORM::factory('User')->where('username','=',$phone)->find();
		if ( ! $orm_user->loaded())
		{
			$orm_user->username = $phone;
			$orm_user->email = $phone.'@crowdhydrology.edu';
			$orm_user->password = 'temp';
			$orm_user->save();
		}

		return User::instance($orm_user);
	}

	public function username($username = NULL)
	{
		return $this->field_string('username', $username);
	}

	public function report($date_start = NULL, $date_end = NULL)
	{
		$orm_readings = $this->orm_object->readings;
		if ($date_start)
		{
			$date_start = date("Y-m-d H:i:s",strtotime($date_start));
			$orm_readings->where('date_time', '>', $date_start);
		}
		if ($date_end)
		{
			$date_end = date("Y-m-d H:i:s",strtotime($date_end));
			$orm_readings->where('date_time', '<', $date_end);
		}
		$orm_readings = $orm_readings->find_all();

		$readings = array();
		foreach ($orm_readings as $orm_reading)
		{
			$readings[] = Reading::instance($orm_reading);
		}

		return $readings;
	}

	public function as_array()
	{
		return $this->orm_object->as_array();
	}

}