<?php

class Vds_Txt extends Source {

	public static function check()
	{
		$gv = new Googlevoice('redranger4oh@gmail.com', '****');

		$texts = $gv->getReadSMS();

		return $texts;
	}

	public static function parse($text)
	{
		$txt = new Txt();
		$phone = str_replace('+', '', $text->phoneNumber);
		$txt->user($phone);
		$txt->station($txt->parse_station($text->messageText));
		$txt->reading($txt->parse_reading($text->messageText));
		return $txt;
	}

}