<?php

class Vds_Controller_Report extends Controller_Website {

	public function action_site_check()
	{
		$station = $this->request->param('station');
		
		$orm_station = ORM::factory('Station')->where('station_id','=', $station)->find();
		
		if ($orm_station->loaded())
		{
			$this->view = array('success'=>true);
		}
		else
		{
			$this->view = array('success'=>false);
		}
	}

	public function action_station()
	{
		$station = $this->request->param('station');
		$date_start = $this->request->param('date_start');
		$date_end = $this->request->param('date_end');

		$orm_station = ORM::factory('Station')->where('station_id','=', $station)->find();
		if ($orm_station->loaded())
		{
			$station = Station::instance($orm_station);

			$readings = $station->report($date_start, $date_end);

		
			if ($this->is_ajax())
			{
				$readings_json = array();
				foreach ($readings as $reading)
				{
					$readings_json[] = $reading->as_array();
				}
				$this->view = $readings_json;
			}
			else
			{
				$data = array();
				
				foreach ($readings as $reading)
				{
					$data[] = $reading->as_array();
				}

				$csv = Csv::build($data, $station->station_id().'.csv')
							->add_column('date_time','Date and Time')
							->add_column('reading','Gage Height (ft)')
							->add_column('station_id','POSIX Stamp')
							->download();
				
				if ($date_start == '' AND $date_end == '')
				{
					$csv->save();
				}
				$this->auto_render = false;
				$this->view = '';
				//$this->view = View::factory('report')->set('readings', $readings);
			}
		}
		else
		{
			$this->view = array('success'=>false);
		}
		
	}

	public function action_phone()
	{
		$phone = $this->request->param('phone');
		$date_start = $this->request->param('date_start');
		$date_end = $this->request->param('date_end');
		
		$orm_user = ORM::factory('User')->where('username','=', $phone)->find();
		if ($orm_user->loaded())
		{
			$user = User::instance($orm_user);

			$readings = $user->report($date_start, $date_end);
		}
		
		if ($this->is_ajax())
		{
			$readings_json = array();
			foreach ($readings as $reading)
			{
				$readings_json[] = $reading->as_array();
			}
			$this->view = $readings_json;
		}
		else
		{
			$this->view = View::factory('report')
							->set('readings', $readings);
		}
	}

	public function action_users()
	{
		$date_start = $this->request->param('date_start');
		$date_end = $this->request->param('date_end');
		
		$orm_users = ORM::factory('User');
		
		if (isset($date_start))
		{
			$date_start = date("Y-m-d H:i:s",strtotime($date_start));
			$orm_users->where('date_created', '>=', $date_start);
		}

		if (isset($date_end))
		{
			$date_end = date("Y-m-d H:i:s",strtotime($date_end));
			$orm_users->where('date_created', '<=', $date_end);
		}
		$orm_users = $orm_users->find_all();
		$users = array();
		if ($this->is_ajax())
		{
			foreach ($orm_users as $orm_user)
			{
				$users[] = User::instance($orm_user)->as_array();
			}
			$this->view = $users;
		}
		else
		{
			foreach ($orm_users as $orm_user)
			{
				$users[] = User::instance($orm_user);
			}
			$this->view = View::factory('reports/users')
									->set('users', $users);
		}
	}

}