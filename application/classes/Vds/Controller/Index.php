<?php defined('SYSPATH') or die('No direct script access.');

class Vds_Controller_Index extends Controller_Website {

	public function action_index()
	{
		$dir = $_SERVER['DOCUMENT_ROOT'].'/data';
		$files = scandir($dir);
		

		$user = User::instance(ORM::factory('User', 1));
		
		foreach ($files as $filename)
		{
			if ($filename != '.' and $filename != '..' and pathinfo($filename, PATHINFO_EXTENSION) == 'csv' and strpos($filename, 'Totals') == false)
			{
				$filepath = $dir.'/'.$filename;
				$station = Station::load(str_ireplace('.csv', '', $filename));
				$last_log = Reading::last_log($station->id());
			
				$csv = fopen($dir.'/'.$filename, "r");

				$i = 0;
				
				while ( ! feof($csv))
				{
					if ($i == 0)
					{
						fgetcsv($csv);
					}
					else
					{
						$data = fgetcsv($csv);
						
						if ($data)
						{
							if ($last_log == FALSE OR strtotime($data[0]) > strtotime($last_log->date_time()))
							{
								Reading::log($user, $station, $data[1], $data[0]);
							}
						}
					}
					$i++;
				}
				
				fclose($csv);

			}
		}
		/*$emails = Gmail::check();

		
		
		foreach ($emails as $email)
		{
			$text = Gmail::parse($email);
			if ($text) {
				try {
					if ($last_log == FALSE OR strtotime($text->date_time()) > strtotime($last_log->date_time()))
					{
						Reading::log($text->user(), $text->station(), $text->reading(), $text->date_time());
					}
				} catch (Exception $e) {
					
				}
			}
			break;
		}
*/
		$this->view = View::factory('index');
	}

}