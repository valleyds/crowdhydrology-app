<?php

class Vds_Controller_Import extends Controller_Website {

	public function action_index()
	{
		if (isset($_GET['import']) AND $_GET['import'] == 'doitnow')
		{
			$files = scandir($_SERVER['DOCUMENT_ROOT'].'/data');
			foreach ($files as $file_name)
			{
				if ($file_name != '.' AND $file_name != '..')
				{
					$station_id = preg_replace('/\\.[^.\\s]{3,4}$/', '', $file_name);
					
					$file = fopen($_SERVER['DOCUMENT_ROOT'].'/data/'.$file_name, 'r');

					$station = Station::load($station_id);
					$user = User::load('5555555555');
					while (($data = fgetcsv($file, 0, ',')) !== FALSE)
					{
						if ($data[0] != 'Date and Time')
						{
							Reading::log($user, $station, $data[1], $data[0]);
						}
					}
				}
				
			}
		}
		
		$this->view = 'Import Page';
	}

}