<?php

abstract class Vds_Source {

	protected $user;

	protected $station;

	protected $reading;

	protected $date_time;

	public function parse_message($message)
	{
		$station_message = preg_split('/\s+/', trim($message));
		return $station_message;
	}

	public function parse_station($message)
	{
		$station_station = $this->parse_message($message);
		return strtoupper($station_station[0]);
	}

	public function parse_reading($message)
	{
		$station_reading = $this->parse_message($message);
		return $station_reading[1];
	}

	public function parse_date_time($message)
	{
		return $message['overview']->date;
	}

	public function user($phone_number = NULL)
	{
		if ($phone_number == NULL)
		{
			return $this->user;
		}
		else
		{
			$this->user = User::load($phone_number);
			return $this;
		}
	}

	public function station($station_id = NULL)
	{
		if ($station_id == NULL)
		{
			return $this->station;
		}
		else
		{
			$orm_station = ORM::factory('Station')->where('station_id','=',$station_id)->find();
			if ($orm_station->loaded())
			{
				$this->station = Station::instance($orm_station);	
				return $this;
			}
			else
			{
				return FALSE;
			}
		}
	}

	public function reading($reading = NULL)
	{
		if ($reading)
		{
			$this->reading = $reading;
			return $this;
		}
		else
		{
			return $this->reading;
		}
	}

	public function date_time($date_time = NULL)
	{
		if ($date_time)
		{
			$this->date_time = $date_time;
			return $this;
		}
		else
		{
			return $this->date_time;
		}
	}

}