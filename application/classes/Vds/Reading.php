<?php

class Vds_Reading extends Entity {

	public static function instance(Model_Reading $orm_reading)
	{
		return new Reading($orm_reading);
	}

	public static function last_log($station_id)
	{
		$sql = "SELECT *
				FROM text_readings
				WHERE station_id = " . $station_id . "
				ORDER BY
					abs(now() - date_time) ASC
				LIMIT 1";
		$query = DB::query(Database::SELECT, $sql)->as_object();
		$result = $query->execute();
		if (isset($result[0]))
		{
			return Reading::instance(ORM::factory('Reading', $result[0]->id));
		}
		else
		{
			return FALSE;
		}
	}

	public static function log(User $user, Station $station, $reading, $date_time)
	{
		$orm_reading = ORM::factory('Reading');
		$orm_reading->user_id = $user->id();
		$orm_reading->station_id = $station->id();
		$orm_reading->reading = $reading;

		$orm_reading->date_time = date('Y-m-d H:i:s', strtotime($date_time));
		$orm_reading->save();
		return Reading::instance($orm_reading);
	}

	public function __construct(Model_Reading $orm_reading)
	{
		$this->orm_object = $orm_reading;
	}

	public function user(User $user = NULL)
	{
		return $this->field_object('user', $user);
	}

	public function station(Station $station = NULL)
	{
		return $this->field_object('station', $station);
	}

	public function date_time($date_time = NULL)
	{
		return $this->field_string('date_time', $date_time);
	}

	public function value($value = NULL)
	{
		return $this->field_string('reading', $value);
	}

	public function as_array()
	{
		$array = parent::as_array();
		$array['date_time'] = date('m/d/Y H:i:s', strtotime($array['date_time']));
		return $array;
	}

}