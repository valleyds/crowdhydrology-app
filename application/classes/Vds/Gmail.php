<?php

class Vds_Gmail extends Source {
	
	protected $host_name = '{imap.gmail.com:993/imap/ssl}INBOX';

	protected $user_name = 'dan@valleyds.com';

	protected $password = '19oucHl3';

	public static function check()
	{
		$gmail = new Gmail();
		return $gmail->check_inbox();
	}

	public static function parse($email)
	{
		try {
			$gmail = new Gmail();
			$from = $email['overview']->from;
			preg_match_all('/\<([^\]]+)\>/', $from, $email_address);
			preg_match_all('!\d+!', $email_address[1][0], $numbers);
			$phone_number = $numbers[0][1];
			$gmail->user($phone_number);

			$message = $email['message'];
			
			$gmail->station($gmail->parse_station($message));
			$gmail->reading($gmail->parse_reading($message));
			$date = $gmail->parse_date_time($email);

			$gmail->date_time($date);
			return $gmail;
		} catch(Exception $e)
		{
			return FALSE;
		}
		
	}

	public function check_inbox()
	{
		$inbox = imap_open($this->host_name, $this->user_name, $this->password);

		$date = date('d F Y', strtotime('-2 days'));

		$emails = imap_search($inbox, 'SINCE "'.$date.'"');
		
		$readings = array();

		if ($emails)
		{
			rsort($emails);
			$i = 0;
			foreach ($emails as $email_number)
			{
				$overview = imap_fetch_overview($inbox, $email_number, 0);
				$message = imap_fetchbody($inbox, $email_number,1);
				
				$readings[$i]['overview'] = $overview[0];
				$readings[$i]['message'] = $message;
				$i++;
				break;
			}
		}
		imap_close($inbox);
		return $readings;
	}

}