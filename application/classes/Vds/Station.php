<?php

class Vds_Station extends Entity {

	public static function load($station_id)
	{
		$station_id = strtoupper($station_id);
		$orm_station = ORM::factory('Station')->where('station_id','=',$station_id)->find();
		if ( ! $orm_station->loaded())
		{
			$orm_station->station_id = $station_id;
			$orm_station->save();
		}

		return Station::instance($orm_station);
	}

	public static function instance(Model_Station $orm_station)
	{
		return new Station($orm_station);
	}

	public function __construct(Model_Station $orm_station)
	{
		$this->orm_object = $orm_station;
	}

	public function report($date_start = NULL, $date_end = NULL)
	{
		$orm_readings = $this->orm_object->readings;
		if ($date_start)
		{
			$date_start = date("Y-m-d H:i:s",strtotime($date_start));
			$orm_readings->where('date_time', '>', $date_start);
		}
		if ($date_end)
		{
			$date_end = date("Y-m-d H:i:s",strtotime($date_end));
			$orm_readings->where('date_time', '<', $date_end);
		}
		$orm_readings = $orm_readings->order_by('date_time', 'ASC')->find_all();

		$readings = array();
		foreach ($orm_readings as $orm_reading)
		{
			$readings[] = Reading::instance($orm_reading);
		}

		return $readings;
	}

	public function station_id($station_id = NULL)
	{
		return $this->field_string('station_id', $station_id);
	}

}