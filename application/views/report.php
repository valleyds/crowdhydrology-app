<table>
	<thead>
		<tr>
			<th>ID</th>
			<th>Phone</th>
			<th>Station</th>
			<th>Date/Time</th>
			<th>Reading</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($readings as $reading) { ?>
			<tr>
				<td><?php echo $reading->id(); ?></td>
				<td><?php echo $reading->user()->username(); ?></td>
				<td><?php echo $reading->station()->station_id(); ?></td>
				<td><?php echo $reading->date_time(); ?></td>
				<td><?php echo $reading->value(); ?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>