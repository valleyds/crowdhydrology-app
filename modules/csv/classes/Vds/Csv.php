<?php

/**
 * Builds and manages CSV files
 */
class Vds_Csv {

	/**
	 * Stores the data that will be in the CSV file
	 * @var array
	 */
	protected $data = NULL;

	/**
	 * If set then its the file that csv will be saved to.
	 * @var string
	 */
	protected $filename = NULL;

	protected $columns = array();

	/**
	 * Accepts the data for creating a new csv
	 * instance.  
	 * 
	 * @param  array $data [description]
	 * @param  string $file [description]
	 * @return object       [description]
	 */
	public static function build(array $data, $file = NULL)
	{
		return new Csv($data, $file);
	}

	public function __construct(array $data, $file = NULL)
	{
		$this->data = $data;
		$this->filename = $file;
	}

	/**
	 * Sets the Headers for displaying a csv directly for download.
	 * @return object
	 */
	public function http_headers($filename)
	{
		/*header('Content-Type: text/csv');
		header("Content-Disposition: attachment; filename=".$filename);
		header("Pragma: no-cache");
		header("Expires: 0");*/

		header('Cache-Control: ');// Avoiding MS Exploder errors
		header('Pragma: ');// Avoiding MS Exploder errors
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);
		return $this;
	}

	/**
	 * Adds a array key from an the loaded data to
	 * use in the csv output.
	 * 
	 * @param string $column
	 * @return object
	 */
	public function add_column($column, $label = NULL)
	{
		$this->columns[] = array(
			'key'	=> $column,
			'label'		=> $label,
		);
		return $this;
	}

	public function html()
	{
		$html = '<table border="1">';
		$html .= '<thead><tr>';
		foreach ($this->columns as $column)
		{
			$html .= '<th>'.$column['label'].'</th>';
		}
		$html .= '</thead></tr>';
		$html .='<tbody>';
		foreach ($this->data as $array_row)
		{
			$html .='<tr>';
			foreach ($this->columns as $column)
			{
				$html .='<td>'.$array_row[$column['key']].'</td>';
			}
			$html .='</tr>';
		}
		$html .='</tbody>';
		$html .= '</table>';

		return $html;
	}

	public function download()
	{
		$this->http_headers(strtoupper($this->filename));
		$file = fopen('php://output','W');

		$this->build_csv($file);

		fclose($file);

		return $this;
	}

	public function save()
	{
		$filename = $_SERVER['DOCUMENT_ROOT'].'/data/'.strtoupper($this->filename);
		try {
			$file = fopen($filename,'w+');

			$this->build_csv($file);		
			
			fclose($file);

			$file = file_get_contents($filename);
			$file = str_replace('"', '', $file);
			$fw = fopen($filename, 'w+');
			fwrite($fw,$file);
			fclose($fw);
		} catch(Exception $e) {
			
		}

		return $this;
	}

	public function build_csv($file)
	{
		$csv = array();
		$columns = array();

		foreach ($this->columns as $column)
		{
			$columns[] = $column['label'];
		}
		
		//fputcsv($fp, $columns);
		fputcsv($file, $columns);
		
		foreach ($this->data as $array_row)
		{
			$row = array();
			foreach ($this->columns as $column)
			{
				$row[] = $array_row[$column['key']];
			}
		//	fputcsv($fp, $row);
			fputcsv($file, $row);
		}

		return $file;
	}

}